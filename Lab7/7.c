#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#define L_B 1024
int main(int argc, char *argv[]){
	int fd1[2],fd2[2], n;
	int d_r1[argc-1],d_r2[argc-1],d_w1[argc-1],d_w2[argc-1];
	pid_t pid;
	char str[L_B]={0};
	char buf[L_B];
	char name[L_B], namef[L_B], file[L_B], MO[L_B];
	int i=0, u=0, j=0;
	char bufer[L_B];
	float mo, ufl,jfl;
	FILE *fp;
	for (i=0; i<argc-1; i++){
		pipe(fd1);
		pipe(fd2);
		d_r1[i]=fd1[0];
		d_w1[i]=fd1[1];
		d_r2[i]=fd2[0];
		d_w2[i]=fd2[1];
	}
	for(i=0; i<argc-1; i++){
		pid = fork();
		if(pid == 0){
			close(d_r1[i]);
			close(d_w2[i]);
			n=read(d_r2[i], namef, sizeof(namef));
			namef[n+1]='\n';
			fp=fopen(namef, "r");

			if (fp==NULL){
				printf("Файл 5055 не найден!!!\n");
				sprintf(MO, "%s\n", "Аларм!!! Аларм!!!");
				write(d_w1[i],MO,(strlen(MO)+1));
				exit(-1);
			}
			while(!feof(fp)){
				fgets(bufer, L_B, fp);
				j++;
			}
			rewind(fp);
			j=j-1;
			int *a;
			a=(int *)malloc(sizeof(int)*j);
			for(int r=0;r<j;r++){
				fgets(buf,L_B,fp);
				a[r]=atoi(bufer);
				u=u+a[r];
			}
			ufl=(float)u;
			jfl=(float)j;
			mo=ufl/jfl;
			sprintf(MO, "Полученное матожидание: %.3f", mo);
			write(d_w1[i],MO,(strlen(MO)+1));
			fclose(fp);
			exit(0);
		}
		else{
			close (d_w1[i]);
			close (d_r2[i]);
			sprintf(name, "%s", argv[i+1]);
			write(d_w2[i], name, (strlen(name)+1));
			read(d_r1[i],buf,sizeof(buf));
			printf("%s\n", buf);
		}
	}
	return(0);
}