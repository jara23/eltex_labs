#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#define L_B 1024
int nclients = 0;
void printusers()
{ 	
	nclients++;
	if(nclients)
	{printf("%d user on-line\n",nclients);}
	else {printf("No User on line\n");}
}
void error(const char *msg)
{
    perror(msg);
    exit(1);
}
float matoj(char file_n[L_B]){
	FILE *fp1;
	int i=0, u=0;
	char buf[L_B], asd[L_B];
	float mo, ufl,ifl;
	for(int xy=0;xy<strlen(file_n)-1;xy++){
		asd[xy]=file_n[xy];
	}
	printf("%s",asd);
	fp1=fopen(asd, "r");
	if (fp1==NULL){
		printf("Файл 5055 не найден!!!\n");
		exit(-1);
	}
	while(!feof(fp1)){
		fgets(buf, L_B, fp1);
		i++;
	}
	rewind(fp1);
	i=i-1;
	int *a;
	a=(int *)malloc(sizeof(int)*i);
	for(int r=0;r<i;r++){
		fgets(buf,L_B,fp1);
		a[r]=atoi(buf);
		u=u+a[r];
	}
	ufl=(float)u;
	ifl=(float)i;
	mo=ufl/ifl;
	printf("\nМатематическое ожидание: %.3f\n",mo);
	fclose(fp1);
	return mo;
}
void dostuff (int sock)
{
	//nclients++;
	int bytes_recv;
	float a;
	char buff[20 * 1024];
	#define str1 "Введите название файла\r\n"
   	write(sock, str1, sizeof(str1));
	bytes_recv = read(sock,&buff[0],sizeof(buff));
	if (bytes_recv < 0) error("Ошибка чтения сокета!!!\n");
	a = matoj(buff);
	snprintf(buff, strlen(buff), "%.3f", a);
	write(sock,&buff[0],sizeof(buff));
	nclients--;
    printf("-disconnect\n"); 
	printusers();
	return;
}
int main(int argc, char *argv[]){
	char buf[L_B];
	printf("Запуск TCP-сервера!!!\n");
	int sfd, sfd_n;
	int p_numb;
	int pid;
	socklen_t clilen;
	struct sockaddr_in server_addr, client_addr;
	if(argc<2){
		fprintf(stderr, "Ошибка, не указан порт!!!\n");
		exit(1);
	}
	sfd=socket(AF_INET, SOCK_STREAM, 0);
	if(sfd<0){
		perror("Ошибка открытия сокета\n");
	}
	bzero((char *)&server_addr, sizeof(server_addr));
	p_numb=atoi(argv[1]);
	server_addr.sin_family=AF_INET;
	server_addr.sin_addr.s_addr=INADDR_ANY;
	server_addr.sin_port=htons(p_numb);
	if(bind(sfd,(struct sockaddr *)&server_addr, sizeof(server_addr))<0){
		perror("Ошибка связи\n");
	}
	listen(sfd,5);
	clilen=sizeof(client_addr);
	while (1){
		sfd_n=accept(sfd,(struct sockaddr *) &client_addr, &clilen);
		if(sfd_n<0) error("Ошибка доступа!!!\n");
		//nclients++;
		//printusers();
		pid=fork();
		if(pid<0) error("Ошибка форка!!!\n");
		if(pid == 0){
			//nclients++;
			printusers();
			close(sfd);
			dostuff(sfd_n);
			exit(-1);
		}
	}
	close(sfd);
}