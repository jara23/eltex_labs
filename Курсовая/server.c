#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define broad "192.168.1.255"
#define L_B 1024
#define port_num_tcp_1 32000
#define port_num_tcp_2 32001
#define port_num_udp_1 32002
#define port_num_udp_2 32003
int count=0;
struct sockaddr_in s_tcp_1_addr, s_tcp_2_addr, s_udp_1_addr, s_udp_2_addr, c_addr;


struct messg
{
	int time;
	int sl;
	char str[10];
	

} mes={0,0,"0"};
void postudp(int s_udp_1){
	char buf[5];
	sprintf(buf,"post");
	while(1){
		sendto(s_udp_1,&buf[0],sizeof(buf),0, (struct sockaddr *) &s_udp_1_addr, sizeof(s_udp_1_addr));
		count++;
		if(count==5){
			return;
		}
	}

}
void getudp(int s_udp_2){
	char buf[4];
	sprintf(buf,"get");
	while(1){
		sendto(s_udp_2,&buf[0],sizeof(buf),0, (struct sockaddr *) &s_udp_2_addr, sizeof(s_udp_2_addr));
		count--;
		if(count==0){
			return;
		}
	}
}
int main(){
	int s_tcp_1, s_tcp_2, s_udp_1, s_udp_2, sock_buf1, sock_buf2; //Объявляем по два дискриптора на каждый протокол
	//int port_num_tcp_1, port_num_tcp_2, port_num_udp_1, port_num_udp_2; //Объявляем переменные для портов для каждого протокола
	int pid;
	int n;
	int bc=1;
	char buffer_str[L_B], str1[4]="post";
	char timebuf[L_B], lstr[L_B], messtr[L_B];
	socklen_t clilen;
	s_tcp_1=socket(AF_INET, SOCK_STREAM, 0);
	s_tcp_2=socket(AF_INET, SOCK_STREAM, 0);
	s_udp_1=socket(AF_INET, SOCK_DGRAM, 0);
	s_udp_2=socket(AF_INET, SOCK_DGRAM, 0);
	if(setsockopt(s_udp_1, SOL_SOCKET, SO_BROADCAST, &bc, sizeof(bc))==-1){
		printf("Флаг бродкаста не поставлен!!!\n");
		exit(-1);
	}
	if(setsockopt(s_udp_2, SOL_SOCKET, SO_BROADCAST, &bc, sizeof(bc))==-1){
		printf("Флаг бродкаста не поставлен!!!\n");
		exit(-1);
	}
	if(s_tcp_1<0){
		perror("Ошибка открытия сокета\n");
	}
	bzero((char *)&s_tcp_1_addr, sizeof(s_tcp_1_addr));
//Сокет для первого tcp порта
	s_tcp_1_addr.sin_family=AF_INET;
	s_tcp_1_addr.sin_addr.s_addr=INADDR_ANY;
	s_tcp_1_addr.sin_port=htons(port_num_tcp_1);
//Сокет для второго tcp порта
	s_tcp_2_addr.sin_family=AF_INET;
	s_tcp_2_addr.sin_addr.s_addr=INADDR_ANY;
	s_tcp_2_addr.sin_port=htons(port_num_tcp_2);
//Сокет для первого udp порта
	s_udp_1_addr.sin_family=AF_INET;
	s_udp_1_addr.sin_addr.s_addr=inet_addr(broad);
	s_udp_1_addr.sin_port=htons(port_num_udp_1);
//Сокет для второго udp порта
	s_udp_2_addr.sin_family=AF_INET;
	s_udp_2_addr.sin_addr.s_addr=inet_addr(broad);
	s_udp_2_addr.sin_port=htons(port_num_udp_2);
	if(bind(s_tcp_1,(struct sockaddr *)&s_tcp_1_addr, sizeof(s_tcp_1_addr))<0){
		perror("Ошибка бинда\n");
		close(s_tcp_1);
		close(s_tcp_2);
		exit(-1);
	}
	if(bind(s_tcp_2,(struct sockaddr *)&s_tcp_2_addr, sizeof(s_tcp_2_addr))<0){
				perror("Ошибка связи\n");
				close(s_tcp_1);
				close(s_tcp_2);
				exit(-1);
	}
//Прослушивание сокетов
	listen(s_tcp_1, 5);
	listen(s_tcp_2, 5);
	clilen=sizeof(c_addr);
	printf("Всё вроде как открылось\n");
	postudp(s_udp_1);
	getudp(s_udp_2);
	/*while(1){
		printf("Зашёл в цикл\n");
		sock_buf1=accept(s_tcp_1,(struct sockaddr *)&c_addr,&clilen);
		send(sock_buf1,&str1[0],strlen(&str1[0]),0);
		n=recv(sock_buf1,&buffer_str[0],sizeof(buffer_str)-1,0);
		printf("Количество байт: %d\n",n);
		if(sock_buf1>0){
			memset(&timebuf[0],0,sizeof(timebuf));
			memset(&lstr[0],0,sizeof(lstr));
			memset(&messtr[0],0,sizeof(messtr));
			printf("Он подключился!!!\n");
			//printf("%s",buffer_str);
			//close(sock_buf1);
			printf("%s\n",buffer_str);
			timebuf[0]=buffer_str[0];
			lstr[0]=buffer_str[1];
			for(int i=2; i<n+1; i++){
				messtr[i-2]=buffer_str[i];
			}
			printf("Время %s, длина %s сообщение %s\n",timebuf,lstr,messtr);
			mes.time=atoi(timebuf);mes.sl=atoi(lstr);strcpy(mes.str,messtr);
			printf("Время сообщения: %d, длина сообщения: %d, полученное сообщение: %s\n",mes.time,mes.sl,mes.str);
			memset(&buffer_str[0], 0, sizeof(buffer_str));
			}
			
			sock_buf2=accept(s_tcp_2,(struct sockaddr *)&c_addr,&clilen);
			sprintf(buffer_str,"get");
			send(sock_buf2,&buffer_str[0],strlen(&buffer_str[0]),0);
			printf("Get оправлен\n");
			sprintf(buffer_str,"%d%d%s",mes.time,mes.sl,mes.str);
			printf("%s\n",buffer_str);
			send(sock_buf2,&buffer_str[0],strlen(&buffer_str[0]),0);
			//close(sock_buf2);*/

	//}

}