#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#define L_B 1024 
struct message
    {
        int time;
        int length;
        char mes[10];
    } m;
int get_random(int min,int max)
{
    int fd = open( "/dev/urandom", O_RDONLY );
    unsigned int result = 0;
    if( fd != -1 )
        read( fd, &result, sizeof(int) );
    close(fd);
    result=min+result%(max-min+1);
    return result;
}
char randommessage(){
    int i,t;
    char mas[L_B];
    t=get_random(1,5);
    i=get_random(1,9);
    printf("%d\n",i);
    for(int a=0; a<i;a++){
       mas[a]=get_random(97,122); 
    }
    m.time=t;
    m.length=i;
    strcpy(m.mes,mas);
}
int main(int argc, char *argv[]){
    int sd, port_number, i=0;
    struct sockaddr_in server_addr;
    struct hostent *server;
    char buffer[L_B], buff;
    printf("Клиент запущен!!!\n");
    randommessage();
    printf("Время сна:%d Длина сообщения:%d Сообщение:%s\n",m.time,m.length,m.mes);
    port_number=32000;
    sd=socket(AF_INET, SOCK_STREAM,0);
    server=gethostbyname(argv[1]);
    bzero((char *)&server_addr, sizeof(server_addr));
    server_addr.sin_family=AF_INET;
    bcopy((char*)server->h_addr,(char *)&server_addr.sin_addr.s_addr, server->h_length);
    server_addr.sin_port=htons(port_number);
    if(connect(sd,(struct sockaddr *)&server_addr,sizeof(server_addr))<0){
        printf("Ошибка соединения!!!\n");
    }
    memset(&buffer,0,sizeof(buffer));
    recv(sd,&buffer[0],strlen(buffer)-1,0);
    //printf("%s\n",buffer );
    if(!strcmp(buffer,"post")){
    sprintf(&buffer[0],"%d%d%s",m.time,m.length,m.mes);
    printf("%s\n",buffer);
    send(sd,&buffer[0],strlen(&buffer[0]),0);
    }
    printf("%s\n",buffer);
    close(sd);
}