#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#define L_B 1024
struct message{
	long type;
	pid_t client;
	char file[L_B];
	float MO;
};
int main(int argc, char *argv[]){
	key_t k;
	int msgid,n;
	struct message mes;
	//FILE *fd;
	k=ftok("./m4.txt",10);
	printf("%d\n",k);
	msgid=msgget(k,0);
	if (msgid<0) {
    	printf("Error: %s\n", strerror(errno));
    	exit(-1);
   }
	mes.type=1;
	mes.client=getpid();
	mes.MO=0;
	printf("%s\n",argv[1]);
	strcpy(mes.file, argv[1]);
	printf("Запрашиваемый файл %s\n", mes.file);
	n=msgsnd(msgid,&mes,sizeof(mes),0);
	if(n<0){
    printf("Битое сообщение\n");
    exit(-1);
   }
   n=msgrcv(msgid,&mes,sizeof(mes),mes.client,0);
   printf("Сообщение %d полученное с сервера %.3f\n",n, mes.MO);
   return 0;
}