#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <errno.h>
#define L_B 1024
struct message{
  long type;
  pid_t client;
  char file[L_B];
  float MO;
};
int main(int argc, char *argv[])
{
key_t k;
int msgid,n,j=0;
struct message mes_in={0,0,"",0}, mes_out;
int i=0, u=0;
char buf[L_B];
float mo, ufl,ifl;
FILE* fd;
k=ftok("./m4.txt",10);
msgid=msgget(k,0600 |IPC_CREAT);
if (msgid<0) {
    printf("Error: %s\n", strerror(errno));
    exit(-1);
}
mes_in.type=1;
mes_in.MO=0;
for(;;){
  mes_in.type=1;
  n=msgrcv(msgid,&mes_in,sizeof(mes_in),mes_in.type, 0);
  if(n<0){
    printf("Bad message received!\n");
    break;
  }
  if (!strcmp("quit",mes_in.file)){
    msgctl(msgid,IPC_RMID,NULL);
    exit(-1);
   }
  if (n>0){
    printf("Message %d\n", n);
    printf("Type %d, file %s\n",mes_in.client, mes_in.file);
    mes_in.file[strlen(mes_in.file)+1]='\n';
    fd=fopen(mes_in.file,"r");
    if (fd==NULL){
      mes_out.MO=-1;
      mes_out.type=mes_in.client;
      msgsnd(msgid,&mes_out,sizeof(mes_out),0);
	continue;
    }
    while(!feof(fd)){
    fgets(buf, L_B, fd);
    i++;
    }
    rewind(fd);
    i=i-1;
    int *a;
    a=(int *)malloc(sizeof(int)*i);
    for(int r=0;r<i;r++){
    fgets(buf,L_B,fd);
    a[r]=atoi(buf);
    u=u+a[r];
    }
    ufl=(float)u;
    ifl=(float)i;
    mo=ufl/ifl;
    printf("Математическое ожидание: %.3f\n",mo);
    fclose(fd);
    mes_out.MO=mo;
    printf("MO %.3f\n",mes_out.MO);
    mes_out.type=mes_in.client;
    n=msgsnd(msgid,&mes_out,sizeof(mes_out),0);
    printf("Message sent to client %d\n", mes_in.client);
    memset(&mes_in,0,sizeof(mes_in));
    memset(&mes_out,0,sizeof(mes_out));
   }
   }
    return 0;
}
