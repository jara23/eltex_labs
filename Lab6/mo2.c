#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define L_B 1024
int main(int argc, char *argv[]){
	FILE *fp1;
	int i=0, u=0;
	char buf[L_B];
	float mo, ufl,ifl;
	fp1=fopen(argv[1], "r");
	if (fp1==NULL){
		printf("Файл 5055 не найден!!!");
		exit(-1);
	}
	while(!feof(fp1)){
		fgets(buf, L_B, fp1);
		i++;
	}
	rewind(fp1);
	i=i-1;
	int *a;
	a=(int *)malloc(sizeof(int)*i);
	for(int r=0;r<i;r++){
		fgets(buf,L_B,fp1);
		a[r]=atoi(buf);
		u=u+a[r];
	}
	ufl=(float)u;
	ifl=(float)i;
	mo=ufl/ifl;
	printf("Математическое ожидание: %.3f\n",mo);
	fclose(fp1);
	return mo;
}