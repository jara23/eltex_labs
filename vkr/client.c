#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#define L_B 1024
#define lh "localhost"
#define udp_port 6666
#define tcp_port 5555
int s_udp, s_tcp, sd_buff;
struct sockaddr_in broadAddr, server_addr;
struct hostent *server;
void error( const char *msg ) {
	perror( msg );
	exit( 1 );
}

//Функция доставки файлов
void delivery(  ) {
	char buffer[L_B], buff[L_B];
	int i;
	FILE *fd;
	fd = fopen( "test.txt", "r" );
	while ( !feof( fd ) ) {
		fgets( buffer, L_B, fd );
		printf( "%s", buffer );
		send( s_tcp, &buffer[0], strlen( &buffer[0] ), 0 );
		recv( s_tcp, &buffer[0], 2, 0 );
		memset( &buffer[0], 0, sizeof( buffer ) );
	}
	fclose( fd );
	close( s_tcp );
	return;
}
int main(  ) {
	int pid, u;
	char buffer[L_B], buff[6];
	int LenStr, n, count = 0;
	socklen_t servlen;
	printf( "Клиетн запущен.\n" );
	if ( ( s_udp = socket( PF_INET, SOCK_DGRAM, 0 ) ) < 0 ) {
		error( "Ошибка открытия UDP-сокета." );
	}
	server = gethostbyname( lh );
	bzero( ( char * ) &server_addr, sizeof( server_addr ) );
	server_addr.sin_family = AF_INET;
	bcopy( ( char * ) server->h_addr,
		   ( char * ) &server_addr.sin_addr.s_addr, server->h_length );
	server_addr.sin_port = htons( tcp_port );
	memset( &broadAddr, 0, sizeof( broadAddr ) );
	broadAddr.sin_family = AF_INET;
	broadAddr.sin_addr.s_addr = htonl( INADDR_ANY );
	broadAddr.sin_port = htons( udp_port );
	servlen = sizeof( server_addr );
	if ( bind
		 ( s_udp, ( struct sockaddr * ) &broadAddr,
		   sizeof( broadAddr ) ) < 0 ) {
		error( "Ошибка связывания" );
	}
	while ( 1 ) {
		if ( ( LenStr = recvfrom( s_udp, buffer, L_B, 0, NULL, 0 ) ) > 0 ) {	//Проверка на получение UDP-пакета
			printf
				( "Получено %d байт по UDP.\nПолученное по UDP сообщение: %s.\n",
				  LenStr, buffer );
			if ( strcmp( buffer, "post\0" ) == 0 ) {	//Сравнение с ключом
				count++;
				printf( "Принят UDP-пакет №%d\n", count );	//
				if ( count == 5 ) {
					count = 0;
					pid = fork(  );
					if ( pid < 0 ) {
						error( "Ошибка форка" );
					}
					if ( pid == 0 ) {
						if ( ( s_tcp =
							   socket( PF_INET, SOCK_STREAM, 0 ) ) < 0 ) {
							error
								( "Ошибка открытия TCP-сокета." );
						}
						if ( connect
							 ( s_tcp, ( struct sockaddr * ) &server_addr,
							   sizeof( server_addr ) ) < 0 ) {
							error( "Ошибка соединения" );
						}
						system( "./cpr.sh" );
						delivery(  );
						printf( "Передача закончена\n" );
						exit( 0 );
					}
				}
			}

		}
	}
	close( s_udp );
}
