#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define L_B 1024
#define bc "192.168.0.255"
#define port_tcp 5555
#define port_udp 6666
struct sockaddr_in server_addr, client_addr, bcst;	//Структуры с адресами сервера и клиентов
int s_tcp, s_udp, sd_buff;		//Дескрипторы сокетов
//Функция, регулирующая время между запросами
void sleeping(  ) {
	FILE *fd;
	char buff[L_B], sl[L_B];
	int i, len;
	fd = fopen( "sleep.cfg", "r" );
	fgets( buff, L_B, fd );
	len = strlen( buff );
	snprintf( sl, len, "%s", buff );
	printf
		( "Перерыв между опросами: %s секунд(ы).\n", sl );
	i = atoi( sl );
	sleep( i );
}

//Функция, отвечающая за получение файла
void delivery(  ) {
	char buffer[L_B];
	int n, u = 0;
	FILE *fd;
	fd = fopen( "test1.txt", "a+" );
	if ( fd == NULL ) {
		printf( "Файл не найден!!!" );
		exit( -1 );
	}
	if ( sd_buff > 0 ) {
//Считывание из сокета
		while ( n = recv( sd_buff, &buffer[0], sizeof( buffer ) - 1, 0 ) > 0 ) {
			printf( "Сообщение: %s", buffer );
			send( sd_buff, "1", 2, 0 );
			fprintf( fd, "%s", buffer );
			memset( &buffer[0], 0, sizeof( buffer ) );
			if ( u == 4 ) {
				fclose( fd );
				printf( "Выполняется перемещение...\n" );
				system( "./fr.sh" );	//Скрипт отвечающий за перемещение и структурирование приходящих файлов
				close( sd_buff );
				return;
			}
			u++;
		}

	}
}

//Функция вывода ошибок
void error( const char *msg ) {
	perror( msg );
	exit( 1 );
}

//Функция отправки UDP-запросов
void postudp(  ) {
	char buf[5];
	int count = 1;
	sprintf( buf, "post" );
	while ( 1 ) {
		sendto( s_udp, &buf[0], sizeof( buf ), 0, ( struct sockaddr * ) &bcst, sizeof( bcst ) );	//Запись в сокет запроса на отправку
		printf( "UDP-пакет №%d отправлен.\n", count );
		count++;
		if ( count > 5 ) {
			return;
		}
		sleep( 1 );
	}
}
int main(  ) {
	printf( "Сервер запущен...\n" );
	int pid;					//Переменная, отвечающая за номер дочернего процесса
	FILE *fd;					//Дескриптор файла
	char buffer[L_B];			//Буфер для приёма сообщений
	socklen_t clilen;
	printf( "Создание сокетов...\n" );
	s_tcp = socket( AF_INET, SOCK_STREAM, 0 );	//Создание TCP-сокета
	if ( ( s_tcp = socket( PF_INET, SOCK_STREAM, 0 ) ) < 0 ) {
		error( "Ошибка открытия TCP-сокета." );
	}
	s_udp = socket( AF_INET, SOCK_DGRAM, 0 );	//Создание UDP-сокета
	if ( ( s_udp = socket( PF_INET, SOCK_DGRAM, 0 ) ) < 0 ) {
		error( "Ошибка открытия UDP-сокета." );
	}
	//Установка флага рассылки по бродкасту
	if ( setsockopt( s_udp, SOL_SOCKET, SO_BROADCAST, &bc, sizeof( bc ) ) == -1 ) {
		error( "Флаг бродкаста не поставлен!!!" );
	}
	//Заполнение структур адресов
	printf( "Заполнение структур...\n" );
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons( port_tcp );
	bcst.sin_family = AF_INET;
	bcst.sin_addr.s_addr = inet_addr( bc );
	bcst.sin_port = htons( port_udp );
	//Привязка сокета к интерфейсу
	if ( bind( s_tcp, ( struct sockaddr * ) &server_addr, sizeof( server_addr ) ) < 0 ) {
		close( s_tcp );
		error( "Ошибка связывания" );
	}
	//Прослушивание сокета
	listen( s_tcp, 5 );
	clilen = sizeof( client_addr );
	sleep( 3 );
	while ( 1 ) {
		sleeping(  );
		printf( "Рассылка UDP-пакетов...\n" );
		postudp(  );
		sd_buff =
			accept( s_tcp, ( struct sockaddr * ) &client_addr, &clilen );
		if ( sd_buff > 0 ) {
			pid = fork(  );
			if ( pid < 0 ) {
				error( "Ошибка форка" );
			}
			if ( pid == 0 ) {
				close( s_tcp );
				delivery(  );
				exit( 0 );
			} else {
				close( sd_buff );
			}
		}
	}
}
