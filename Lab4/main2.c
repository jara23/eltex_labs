#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define L_B 1024
int main (int argc, char *argv[]){
	int x, n, count=0, m;
	char buf[L_B];
	FILE *fp1, *fp2;
	fp1=fopen(argv[1], "r");
	fp2=fopen(argv[2], "w");
	//n=atoi(argv[3]);
	if (fp1==NULL){
		printf("Файл не найден!!!");
		exit(-1);
	}
	if (fp2==NULL)
	{
		printf("Нет доступа!!!");
		exit(-1);
	}
	if (argv[3]==NULL){
		printf("Нет ключа!!!");
		exit(-1);
	}
	while(!feof(fp1)){
		for(m=0;m<L_B; m++){
			buf[m]=fgetc(fp1);
			if(buf[m]==EOF){
				break;
		}			
			if(buf[m] != argv[3][0])
			{
				fputc(buf[m], fp2);			
			}
			}
	}
	fclose(fp1);
	fclose(fp2);
	return 0;
}	
	