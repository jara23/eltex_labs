#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/sem.h>
#define L_B 1024
union semun {
 int val;                  /* значение для SETVAL */
 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
 unsigned short *array;    /* массивы для GETALL, SETALL */
                           /* часть, особенная для Linux: */
 struct seminfo *__buf;    /* буфер для IPC_INFO */
}arg;
int main(int argc, int *argv[])
{
    struct sembuf for_s={0,-1,0};
    struct sembuf for_c={1,1,0};
    char c, *n, buf[L_B];
   key_t k;
   int cshmid,ec=0,sem=0, u=0,i=0;
   char mas[10];
   float MO=0, ufl, ifl;
 FILE* fd;
 arg.val=0;
 k=ftok("/etc/fstab",10);
 sem=semget(k,2,0600|IPC_CREAT);
 semctl(sem,0,SETVAL,arg);
 semctl(sem,1,SETVAL,arg);
 cshmid=shmget(k,sizeof(float)+10,0600 |IPC_CREAT);
   if (cshmid<0) {
    printf("Error: %s\n", strerror(errno));
    exit(-1);
   }
   printf("MO before %.3f\n", MO);
   MO=0;
   for(;;){

    n=(char*)shmat(cshmid,0,0);

   if(n<0){
    printf("Bad message received!\n");
    break;
   }
   memset(n,0,sizeof(float)+10);
   if(semop(sem, &for_s,1)==-1){
    printf("Error while blocking sem\n");
   }
   strcpy(mas,n);
   if (!strcmp("exit",mas)){
    printf("Exitting...\n");
    MO=0;
    sprintf(n, "%.3f\n", MO);
     semop(sem, &for_c,1);
       if(semop(sem,&for_s,1)<1){
memset(n,0,sizeof(float)+10);
exit(0);
        }
   }
   printf("mas %s\n",mas);
   if (n>0){
        printf("Message %s\n", mas);
        mas[strlen(mas)+1]='\n';
    fd=fopen(mas,"r");
    if (fd==NULL){
        MO=-1;
        memcpy(n, "File not found",sizeof(char)*14);
        printf("NULL\n");
        break;
    }
    memset(n, 0, sizeof(float)+10);
if (fd==NULL){
    printf("Файл 5055 не найден!!!");
    exit(-1);
  }
  while(!feof(fd)){
    fgets(buf, L_B, fd);
    i++;
  }
  rewind(fd);
  i=i-1;
  int *a;
  a=(int *)malloc(sizeof(int)*i);
  for(int r=0;r<i;r++){
    fgets(buf,L_B,fd);
    a[r]=atoi(buf);
    u=u+a[r];
  }
  ufl=(float)u;
  ifl=(float)i;
  MO=ufl/ifl;
  printf("Математическое ожидание: %.3f\n",MO);
  fclose(fd);
   printf("MO %.3f\n",MO);
        sprintf(n, "%.3f\n", MO);
        semop(sem, &for_c,1);
        printf("Message sent to client \n");

        if(semop(sem,&for_s,1)<1){
memset(n,0,sizeof(float)+10);

        }
MO=0;
c=0;
u=0; ufl=0; ifl=0; i=0;
   }
   }
    return 0;
}